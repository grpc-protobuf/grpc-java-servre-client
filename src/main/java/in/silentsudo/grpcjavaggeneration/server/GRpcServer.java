package in.silentsudo.grpcjavaggeneration.server;

import io.grpc.Server;
import io.grpc.ServerBuilder;

import java.io.IOException;

public class GRpcServer {
    public static void main(String[] args) throws IOException, InterruptedException {
        System.out.println("Hello gRPC Server");

        Server server = ServerBuilder.forPort(50051)
                .build();


        server.start();

        Runtime.getRuntime()
                .addShutdownHook(new Thread(() -> {
                    System.out.println("Got shutdown command");
                    server.shutdown();
                    System.out.println("Server terminated successfully");
                }));

        server.awaitTermination();
    }
}
