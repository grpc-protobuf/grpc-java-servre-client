package in.silentsudo.grpcjavaggeneration.client;

import in.silentsudo.grpcjava.MessageServiceGrpc;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;

public class GRpcClient {

    public static void main(String[] args) {
        System.out.println("gRPC client");

        ManagedChannel messageServiceChannel = ManagedChannelBuilder.forAddress("locaclhost", 50051)
                .build();

        MessageServiceGrpc.MessageServiceBlockingStub client = MessageServiceGrpc.newBlockingStub(messageServiceChannel);

    }
}
